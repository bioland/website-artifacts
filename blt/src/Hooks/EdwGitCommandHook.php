<?php

namespace Acquia\Blt\Custom\Hooks;

use Acquia\Blt\Robo\BltTasks;

/**
 * Class EdwGitCommandHook implements EdW specific git command overrides.
 *
 * @package Acquia\Blt\Custom\Hooks
 */
class EdwGitCommandHook extends BltTasks {

  /**
   * Override git commit hook.
   *
   * @param string $message
   *   Git message string.
   *
   * @hook replace-command git:commit-msg
   *
   * @return int
   *   status 0 means success
   */
  public function commitMsgHook($message) {
    $this->say('Validating commit message syntax...');
    if (!preg_match("/\#\d+/", $message)) {
      $this->logger->error("Invalid commit message!");
      $this->say("Example: #135: Added the new field to the page content type.");
      return 1;
    }

    return 0;
  }

  /**
   * Validates staged files.
   *
   * @param string $changed_files
   *   A list of staged files, separated by \n.
   *
   * @hook replace-command git:pre-commit
   */
  public function preCommitHook($changed_files) {
    $this->invokeCommands([
      // Passing a file list to be PHPCS will cause all specified files to
      // be sniffed, regardless of the extensions or patterns defined in
      // phpcs.xml. So, we do not use validate:phpcs:files.
      'validate:phpcs:files' => ['file_list' => $changed_files],
      'validate:twig:files' => ['file_list' => $changed_files],
      'validate:yaml:files' => ['file_list' => $changed_files],
    ]);

    $changed_files_list = explode("\n", $changed_files);
    if (in_array(['composer.json', 'composer.lock'], $changed_files_list)) {
      $this->invokeCommand('validate:composer', ['file_list' => $changed_files]);
    }

    $this->invokeHook('pre-commit');
    $this->say("<info>Your local code has passed git pre-commit validation.</info>");
  }

}
